from flask_script import Manager

# from app.app import app
from app.run import app
from manage.commands import CreateDatabaseCommand, DropDatabaseCommand, ApplyMigratesDatabaseCommand

manager = Manager(app)
manager.add_command('create-database', CreateDatabaseCommand())
manager.add_command('drop-database', DropDatabaseCommand())
manager.add_command('migrate-database', ApplyMigratesDatabaseCommand())

if __name__ == '__main__':
    manager.run()
