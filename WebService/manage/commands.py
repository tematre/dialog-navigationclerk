import docker
import psycopg2
from flask_script import Command
from flask_script import Option
from yoyo import read_migrations, get_backend


class CreateDatabaseCommand(Command):
    "Creates database either in docker container or on local DBMS"
    option_list = (
        Option('--docker', '-d', dest='in_docker', default=False),
        Option('--host', '-h', dest='host', default='localhost'),
        Option('--port', '-p', dest='port', default='5432'),
        Option('--dbname', '-n', dest='dbname', default='postgres'),
        Option('--container', '-c', dest='container_name', default='PostgressDevDB'),
        Option('--username', '-u', dest='username', default='postgres'),
        Option('--password', '-s', dest='password', default='postgres')
    )
    def run(self, in_docker, username, password, container_name, host, port, dbname):
        if in_docker:
            self.create_docker(container_name, password, port)
        else:
            self.create_postgres(host, port, username, password, dbname)

    def create_docker(self, container_name, password, mapped_port):
        client = docker.from_env()
        existedContainer = next((e for e in client.containers.list(all=True) if e.name == container_name), None)
        if existedContainer is not None:
            print("Docker container with database already exists. Please delete it if you want to recreate database")
            return
        print("Binding postgres to port " + mapped_port)
        client.containers.run('postgres:latest', name=container_name, detach=True,
                              environment=["POSTGRES_PASSWORD=" + password], ports={'5432/tcp': ('0.0.0.0', mapped_port)})
        print("Database successfully created")

    def create_postgres(self, host, port, username, password, dbname):
        connection_string = "host="+host + " port=" + port + " user=" + username + " password=" + password
        conn = psycopg2.connect(connection_string)
        conn.autocommit = True
        query = "CREATE DATABASE " + dbname
        cursor = conn.cursor()
        cursor.execute(query)
        conn.autocommit = False
        print("Database successfully created")


class DropDatabaseCommand(Command):
    "Deletes database either in docker container or on local DBMS"
    option_list = (
        Option('--docker', '-d', dest='in_docker', default=False),
        Option('--host', '-h', dest='host', default='localhost'),
        Option('--port', '-p', dest='port', default='5432'),
        Option('--dbname', '-n', dest='dbname', default='postgres'),
        Option('--container', '-c', dest='container_name', default='PostgressDevDB'),
        Option('--username', '-u', dest='username', default='postgres'),
        Option('--password', '-s', dest='password', default='postgres')
    )
    def run(self, in_docker, username, password, container_name, host, port, dbname):
        if in_docker:
            self.remove_docker(container_name)
        else:
            self.remove_postgres(host, port, username, password, dbname)

    def remove_docker(self, container_name):
        client = docker.from_env()
        existedContainer = next((e for e in client.containers.list(all=True) if e.name == container_name), None)
        if existedContainer is None:
            print("Docker container not found.")
            return
        existedContainer.stop()
        existedContainer.remove()
        print("Docker database was removed.")

    def remove_postgres(self, host, port, username, password, dbname):
        connection_string = "host="+host + " port=" + port + " user=" + username + " password=" + password
        conn = psycopg2.connect(connection_string)
        conn.autocommit = True
        query = "DROP DATABASE " + dbname
        cursor = conn.cursor()
        cursor.execute(query)
        conn.autocommit = False
        print("Postgres database was removed.")


class ApplyMigratesDatabaseCommand(Command):
    "Applies migrates to specified database"
    option_list = (
        Option('--host', '-h', dest='host', default='localhost'),
        Option('--port', '-p', dest='port', default='5432'),
        Option('--dbname', '-n', dest='dbname', default='postgres'),
        Option('--username', '-u', dest='username', default='postgres'),
        Option('--password', '-s', dest='password', default='postgres')
    )
    def run(self, host, port, dbname, username, password):
        connection_string = 'postgres://' + username + ':' + password + '@' + host + ':' + port + '/' + dbname
        backend = get_backend(connection_string)
        migrations = read_migrations('./migrations')
        backend.apply_migrations(backend.to_apply(migrations))
        print("Database was migrated.")
