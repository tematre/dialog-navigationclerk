"""
Initial migration
"""

from yoyo import step

__depends__ = {}

steps = [
    # Tables
    step(
        "CREATE TABLE actions (id int PRIMARY KEY, basic_name varchar, parent_action_id int, priority int, " +
        "strategy varchar, strategy_data varchar, average_data varchar, max_deviation float)"),
    step("CREATE TABLE structure_items (id int PRIMARY KEY, parent_id int, type_id int, order_num int )"),
    step("CREATE TABLE structure_actions (structure_id int, action_id int)"),
    step("CREATE TABLE structure_types (id int PRIMARY KEY, name varchar)"),
    step("CREATE TABLE roles (id int PRIMARY KEY, name varchar(80))"),
    step(
        "CREATE TABLE users (id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY, guid varchar(100) UNIQUE, sex boolean, age int)"),
    step("CREATE TABLE user_roles (user_id int, role_id int)"),
    step("CREATE TABLE dialog_states (id int PRIMARY KEY, name varchar(80))"),
    step("CREATE TABLE dialog_message_response_type (id int PRIMARY KEY, name varchar(80))"),
    step(
        "CREATE TABLE dialogs (id int PRIMARY KEY, user_id int, state_id int, initial_place varchar(200), expire_date bigint)"),
    step(
        "CREATE TABLE dialog_messages (id int PRIMARY KEY, dialog_id int, message varchar(1000), response_type_id int )"),
     step(
        "ALTER TABLE dialog_messages ADD CONSTRAINT fk_dialog_messages_response_type_id FOREIGN KEY (response_type_id) REFERENCES dialog_message_response_type (id)"),
    
    step(
        "CREATE TABLE dialog_messages_actions (message_id int, action_id int)"),
    
    # Composite primary keys
    step("ALTER TABLE structure_actions ADD CONSTRAINT structure_actions_pk PRIMARY KEY (structure_id, action_id)"),
    step("ALTER TABLE user_roles ADD CONSTRAINT user_roles_pk PRIMARY KEY (user_id, role_id)"),
    # Foreign keys
    step(
        "ALTER TABLE structure_items ADD CONSTRAINT fk_structure_items_id_parent_id FOREIGN KEY (parent_id) REFERENCES structure_items (id)"),
    step(
        "ALTER TABLE structure_actions ADD CONSTRAINT fk_structure_actions_structure_id FOREIGN KEY (structure_id) REFERENCES structure_items (id)"),
    step(
        "ALTER TABLE structure_actions ADD CONSTRAINT fk_structure_actions_action_id FOREIGN KEY (action_id) REFERENCES actions (id)"),
    step(
        "ALTER TABLE structure_items ADD CONSTRAINT fk_structure_items_type_id FOREIGN KEY (type_id) REFERENCES structure_types (id)"),
    step(
        "ALTER TABLE actions ADD CONSTRAINT fk_actions_parent_action_id FOREIGN KEY (parent_action_id) REFERENCES actions (id)"),
    step("ALTER TABLE user_roles ADD CONSTRAINT fk_user_roles_user_id_users_id FOREIGN KEY (user_id) REFERENCES users (id)"),
    step("ALTER TABLE user_roles ADD CONSTRAINT fk_user_roles_role_id_roles_id FOREIGN KEY (role_id) REFERENCES roles (id)"),
    step("ALTER TABLE dialogs ADD CONSTRAINT fk_dialogs_user_id_users_id FOREIGN KEY (user_id) REFERENCES users (id)"),
    step("ALTER TABLE dialogs ADD CONSTRAINT fk_dialogs_state_id_dialog_states_id FOREIGN KEY (state_id) REFERENCES dialog_states (id)"),
    step("ALTER TABLE dialog_messages ADD CONSTRAINT fk_dialog_messages_dialog_id_dialogs_id FOREIGN KEY (dialog_id) REFERENCES dialogs (id)"),
    step("ALTER TABLE dialog_messages_actions ADD CONSTRAINT fk_dialog_messages_actions_message_id FOREIGN KEY (message_id) REFERENCES dialog_messages(id)"),
    step("ALTER TABLE dialog_messages_actions ADD CONSTRAINT fk_dialog_messages_actions_action_id FOREIGN KEY (action_id) REFERENCES actions(id)")
]
