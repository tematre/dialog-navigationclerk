from injector import singleton, InstanceProvider
from config import Configuration, ConfigurationSet, config_from_env, config_from_ini

from app.bll.bert_strategy import BertStrategy
from app.bll.intent_recognition_service import IntentRecognitionService
from app.bll.sentiment_analysis_service import SentimentAnalysisService
from app.bll.bert_sentiment_model import BertSentimentModel
from app.bll.dialog_service import DialogService
from app.bll.rubert_strategy import RubertStrategy
from app.bll.sbert_strategy import SbertStrategy


def configure(binder):
    binder.bind(IntentRecognitionService, to=IntentRecognitionService, scope=singleton)
    binder.bind(SentimentAnalysisService, to=SentimentAnalysisService, scope=singleton)
    binder.bind(BertSentimentModel, to=BertSentimentModel, scope=singleton)
    binder.bind(DialogService, to=DialogService, scope=singleton)
    binder.bind(BertStrategy, to=BertStrategy, scope=singleton)
    binder.bind(SbertStrategy, to=SbertStrategy, scope=singleton)
    binder.bind(RubertStrategy, to=RubertStrategy, scope=singleton)
    binder.bind(Configuration, to=InstanceProvider(ConfigurationSet(
        config_from_env(prefix="_"),
        # config_from_ini("./app/resources/configs/config.ini", read_from_file=True)
        config_from_ini(
            "./app/resources/configs/config.ini",
            read_from_file=True)
    )))
