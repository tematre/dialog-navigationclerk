from config import Configuration as ConfigFromLib
from injector import inject


@inject
class Configuration:
    def __init__(self, config: ConfigFromLib):
        self.config = config

    def getConnectionString(self):
        return self.config['database.connection_string']