from flask import Flask
from flask_injector import FlaskInjector
from injector import inject


from app.api.techService import tech_api
from app.api.user_api import user_api
from app.common.dependencies import configure
from app.api.dialog import dialog_api
from app.api.test import test_api

app = Flask(__name__)

app.register_blueprint(dialog_api)
app.register_blueprint(user_api)
app.register_blueprint(tech_api)
app.register_blueprint(test_api)

FlaskInjector(app=app, modules=[configure])

if __name__ == '__main__':
    app.run()
