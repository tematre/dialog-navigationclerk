import psycopg2
from injector import inject

from app.dal.entities.user import User
from app.common.configuration import Configuration

@inject
class UsersRepository:
    #def __init__(self):
    def __init__(self, config: Configuration):
        self.session = psycopg2.connect(config.getConnectionString())
        #self.session = psycopg2.connect(database="test", user="postgres", password="12345")

    def create_user(self, guid, sex, age):
        try:
            cur = self.session.cursor()
            sql = 'INSERT INTO users(guid, sex, age) VALUES (%s, %s, %s)'
            cur.execute(sql, (guid, sex, age))
            self.session.commit()
        finally:
            if cur:
                cur.close()

    def find_user_by_guid(self, guid):
        try:
            cur = self.session.cursor()
            sql = 'SELECT * FROM users WHERE guid = %s'
            cur.execute(sql, (guid, ))
            row = cur.fetchone()
            if row is None:
                return None

            user = User(row[0], row[1], row[2], row[3])
            return user
        finally:
            if cur:
                cur.close()

    def delete_user_by_guid(self, guid):
        try:
            cur = self.session.cursor()
            sql = 'DELETE FROM users WHERE guid = %s'
            cur.execute(sql, (guid, ))
            self.session.commit()
        finally:
            if cur:
                cur.close()
