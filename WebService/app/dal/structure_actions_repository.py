import psycopg2
from injector import inject

from app.common.configuration import Configuration
from app.dal.entities.structure_actions import StructureActions


@inject
class StructureActionsRepository():
    #def __init__(self):
    def __init__(self, config: Configuration):
        self.session = psycopg2.connect(config.getConnectionString())
        #self.session = psycopg2.connect(database="test", user="postgres", password="12345")

    def find_all_structure_actions(self):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT structure_id, action_id  FROM "structure_actions"'
            cur.execute(postgreSQL_select_Query)
            print("Selecting all rows from structure_actions table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_action = StructureActions(row[0], row[1])
                result_list.append(current_action)

            return result_list
        finally:
            if (cur):
                cur.close()

    def find_structure_actions_by_structure_id(self, structure_id):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT structure_id, action_id FROM "structure_actions" WHERE structure_id = %s'
            cur.execute(postgreSQL_select_Query, (structure_id,))
            print("Selecting rows from structure_actions table by id " + str(structure_id) + " using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_action = StructureActions(row[0], row[1])
                result_list.append(current_action)

            return result_list
        finally:
            if (cur):
                cur.close()

    def find_structure_actions_by_action_id(self, action_id):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT * FROM structure_actions WHERE action_id = %s'
            cur.execute(postgreSQL_select_Query, (action_id,))
            print(
                "Selecting all rows from structure_actions table by action_id " + str(action_id) + " using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_action = StructureActions(row[0], row[1])
                result_list.append(current_action)

            return result_list
        finally:
            if (cur):
                cur.close()
