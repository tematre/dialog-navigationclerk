class Role:
    id: int
    name: str

    @property
    def _name(self):
        return self.name

    @_name.setter
    def _name(self, name):
        self.name = name
