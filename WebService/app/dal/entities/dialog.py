from datetime import datetime


class Dialog:
    id: int
    user_id: int
    state_id: int
    initial_place: str
    intent_ids: list
    expire_date: datetime

    def __init__(self, id, user_id, state_id, initial_place, intent_ids, expire_date):
        self.id = id
        self.user_id = user_id
        self.state_id = state_id
        self.initial_place = initial_place
        self.intent_ids = intent_ids
        self.expire_date = expire_date

    @property
    def _user_id(self):
        return self.user_id

    @_user_id.setter
    def _user_id(self, user_id):
        self.user_id = user_id

    @property
    def _state_id(self):
        return self.state_id

    @_state_id.setter
    def _state_id(self, state_id):
        self.state_id = state_id

    @property
    def _initial_place(self):
        return self.initial_place

    @_initial_place.setter
    def _initial_place(self, place):
        self.initial_place = place

    @property
    def _intent_ids(self):
        return self.intent_ids

    @_intent_ids.setter
    def _intent_ids(self, ids):
        self.intent_ids = ids
