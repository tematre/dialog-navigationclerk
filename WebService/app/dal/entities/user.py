class User:
    id: int
    guid: str
    sex: bool
    age: int

    def __init__(self, id, guid, sex, age):
        self.id = id
        self.guid = guid
        self.sex = sex
        self.age = age

    @property
    def _guid(self):
        return self.guid

    @_guid.setter
    def _guid(self, guid):
        self.guid = guid

    @property
    def _sex(self):
        return self.sex

    @_sex.setter
    def _sex(self, sex):
        self.sex = sex

    @property
    def _age(self):
        return self.age

    @_age.setter
    def _age(self, age):
        self.age = age
