import enum
class DialogMessageResponseType(enum.Enum):
    ClarificationRequired = 1
    IntentRecognized = 2
    DialogEnded = 2