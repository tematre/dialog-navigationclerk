class Setting(object):
    _name: str
    _value: str

    def __init__(self, name, value):
        self._name = name
        self._value = value

    def __init__(self):
        self._name = ''
        self._value = ''

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, v):
        self._name = v

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        self._value = v
