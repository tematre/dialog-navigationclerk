class StructureItems:
    id: int
    parent_id: int
    type_id: int
    order_num: int

    def __init__(self, id, parent_id, type_id, order_num):
        self.id = id
        self.parent_id = parent_id
        self.type_id = type_id
        self.order_num = order_num

    @property
    def _id(self):
        return self.id

    @_id.setter
    def _id(self, id):
        self.id = id

    @property
    def _type_id(self):
        return self.type_id

    @_type_id.setter
    def _type_id(self, type_id):
        self.type_id = type_id

    @property
    def _parent_id(self):
        return self.parent_id

    @_parent_id.setter
    def _parent_id(self, parent_id):
        self.parent_id = parent_id
