class Action:
    basic_name: str
    parent_action_id: str
    priority: int
    strategy: str
    strategy_data: str
    average_data: str
    max_deviation: float

    def __init__(self, basic_name, parent_action_id, priority, strategy, strategy_data, average_data, max_deviation):
        self.basic_name = basic_name
        self.parent_action_id = parent_action_id
        self.priority = priority
        self.strategy = strategy
        self.strategy_data = strategy_data
        self.average_data = average_data
        self.max_deviation = max_deviation

    @property
    def _basic_name(self):
        return self.basic_name

    @_basic_name.setter
    def _basic_name(self, name):
        self.basic_name = name

    @property
    def _parent_action_id(self):
        return self.parent_action_id

    @_parent_action_id.setter
    def _parent_action_id(self, parent_action_id):
        self.parent_action_id = parent_action_id

    @property
    def _priority(self):
        return self.priority

    @_priority.setter
    def _priority(self, new_priority):
        self.priority = new_priority

    @property
    def _strategy(self):
        return self.strategy

    @_strategy.setter
    def _strategy(self, strategy):
        self.strategy = strategy

    @property
    def _strategy_data(self):
        return self.strategy_data

    @_strategy_data.setter
    def _strategy_data(self, new_data):
        self.strategy_data = new_data

    @property
    def _average_data(self):
        return self.average_data

    @_average_data.setter
    def _average_data(self, average_data):
        self.average_data = average_data

    @property
    def _max_deviation(self):
        return self.max_deviation

    @_max_deviation.setter
    def _max_deviation(self, max_deviation):
        self.max_deviation = max_deviation
