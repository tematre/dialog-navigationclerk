class StructureActions:
    structure_id: int
    action_id: int

    def __init__(self, structure_id, action_id):
        self.structure_id = structure_id
        self.action_id = action_id

    @property
    def _structure_id(self):
        return self.structure_id

    @_structure_id.setter
    def _structure_id(self, structure_id):
        self.structure_id = structure_id

    @property
    def _action_id(self):
        return self.action_id

    @_action_id.setter
    def _action_id(self, action_id):
        self.action_id = action_id
