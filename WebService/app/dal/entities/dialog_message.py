class DialogMessage:
    id: int
    dialog_id: int
    direction_id: int
    message: str
    order_number: int

    @property
    def _dialog_id(self):
        return self.dialog_id

    @_dialog_id.setter
    def _dialog_id(self, dialog_id):
        self.dialog_id = dialog_id

    @property
    def _direction_id(self):
        return self.direction_id

    @_direction_id.setter
    def _direction_id(self, direction_id):
        self.direction_id = direction_id

    @property
    def _message(self):
        return self.message

    @_message.setter
    def _message(self, message):
        self.message = message

    @property
    def _order_number(self):
        return self.order_number

    @_order_number.setter
    def _order_number(self, order_number):
        self.order_number = order_number
