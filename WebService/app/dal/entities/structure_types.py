class StructureTypes:
    id: int
    name: str

    def __init__(self, id, name):
        self.id = id
        self.name = name

    @property
    def _id(self):
        return self.id

    @_id.setter
    def _id(self, id):
        self.id = id

    @property
    def _name(self):
        return self.name

    @_name.setter
    def _name(self, name):
        self.name = name
