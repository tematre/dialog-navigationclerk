import psycopg2
from injector import inject

from app.common.configuration import Configuration
from app.dal.entities.structure_actions import StructureActions
from app.dal.entities.structure_items import StructureItems


@inject
class StructureItemsRepository():
    #def __init__(self):
    def __init__(self, config: Configuration):
        self.session = psycopg2.connect(config.getConnectionString())
        #self.session = psycopg2.connect(database="test", user="postgres", password="12345")

    def find_all_structure_items(self):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT id, parent_id, type_id, order_num  FROM "structure_items"'
            cur.execute(postgreSQL_select_Query)
            print("Selecting all rows from structure_items table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_item = StructureItems(row[0], row[1], row[2], row[3])
                result_list.append(current_item)
            return result_list
        finally:
            if (cur):
                cur.close()

    def find_structure_item_by_id(self, id):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT id, parent_id, type_id, order_num FROM "structure_items" WHERE id = %s'
            cur.execute(postgreSQL_select_Query, (id,))
            print("Selecting all rows from structure_items table by id " + str(id) + " using cursor.fetchone")
            row = cur.fetchone()
            current_item = StructureItems(row[0], row[1], row[2], row[3])
            return current_item
        finally:
            if (cur):
                cur.close()

    def find_structure_items_by_parent_id(self, parent_id):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT id, parent_id, type_id, order_num FROM "structure_items" WHERE parent_id = %s'
            cur.execute(postgreSQL_select_Query, (parent_id,))
            print(
                "Selecting all rows from structure_actions table by parent_id " + str(parent_id) + " using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_item = StructureItems(row[0], row[1], row[2], row[3])
                result_list.append(current_item)
            return result_list
        finally:
            if (cur):
                cur.close()
