import datetime

import psycopg2
from injector import inject

from app.dal.entities.dialog import Dialog
from app.common.configuration import Configuration


@inject
class DialogsRepository:
    def __init__(self, config: Configuration):
        # self.session = psycopg2.connect(config.getConnectionString())
        self.session = psycopg2.connect(config.getConnectionString())

    # def find_all_dialogs(self):
    #     cur = self.session.cursor()
    #     postgreSQL_select_Query = 'SELECT * FROM "dialogs"'
    #     cur.execute(postgreSQL_select_Query)
    #     print("Selecting rows from dialogs table using cursor.fetchall")
    #     dialogs_records = cur.fetchall()
    #     result_list = list()
    #     for row in dialogs_records:
    #         dialog =
    #     return result_list

    def find_dialog_by_id(self, id):
        cur = self.session.cursor()
        postgreSQL_select_Query = "SELECT * FROM dialogs WHERE id = %s"
        cur.execute(postgreSQL_select_Query, (id,))

        print("Selecting rows from dialogs table using cursor.fetchone")
        row = cur.fetchone()

        res = Dialog(row[0], row[1], row[2], row[3], [], datetime.datetime.fromtimestamp(row[4]))
        return res
    
    def create_new_dialog(self, user_id, expire_date):
        cur = self.session.cursor()
        sql = 'INSERT INTO dialogs(user_id, state_id, initial_place, expire_date) VALUES (%s, %s, %s, %s)'
        cur.execute(sql, (user_id, 1, '1', expire_date))
        self.session.commit()

       
    def find_last_dialog_by_user_id(self, id):
        cur = self.session.cursor()
        postgreSQL_select_Query = "SELECT * FROM dialogs WHERE user_id = %s ORDER BY expire_date"
        cur.execute(postgreSQL_select_Query, (id,))

        print("Selecting rows from dialogs table using cursor.fetchone")
        row = cur.fetchone()

        if row is None:
            return None

        res = Dialog(row[0], row[1], row[2], row[3], [], datetime.datetime.fromtimestamp(row[4]))
        return res
    
    def find_dialog_top_level_intents(self, dialogId):
        cur = self.session.cursor()
        postgreSQL_select_Query = "SELECT dma.* FROM dialog_messages_actions dma INNER JOIN dialog_messages dm WHERE dm.dialog_id=%s"
        cur.execute(postgreSQL_select_Query, (dialogId,))

        print("Selecting rows from dialogs table using cursor.fetchone")
        
        top_level_intent_ids =  [x[0] for x in cur.fetchall()]


    def update_dialog_state(self, id, state):
        cur = self.session.cursor()
        sql = 'UPDATE dialogs SET state_id=%s WHERE id = %s'
        cur.execute(sql, (state, id))
        self.session.commit()
    
    def save_dialog_message(self, dialog_id, text, response_type):
        cur = self.session.cursor()
        sql = 'INSERT dialog_messages (dialog_id, message, response_type_id) VALUES (%s, %s, %s)'
        cur.execute(sql, (dialog_id, text, response_type))
        self.session.commit()
    
    def save_dialog_message_action(self, dialog_message_id, action_id):
        cur = self.session.cursor()
        sql = 'INSERT dialog_messages_actions (message_id, action_id) VALUES (%s, %s)'
        cur.execute(sql, (dialog_message_id, action_id))
        self.session.commit()

    #
    # def find_dialogs_by_user_id(self, user_id):
    #     cur = self.session.cursor()
    #     postgreSQL_select_Query = 'SELECT * FROM "dialogs" WHERE user_id=?'
    #     cur.execute(postgreSQL_select_Query, (user_id,))
    #     print("Selecting rows from dialogs table using cursor.fetchall")
    #     dialogs_records = cur.fetchall()
    #     for row in dialogs_records:
    #
    #     return rows
