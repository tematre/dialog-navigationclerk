import psycopg2
from injector import inject

from app.common.configuration import Configuration
from app.dal.entities.actions import Action


@inject
class ActionsRepository:
    def __init__(self, config: Configuration):
        # def __init__(self, config: Configuration):
        self.session = psycopg2.connect(config.getConnectionString())
        #self.session = psycopg2.connect(dhost=config.getHost(), port=config.getPort(), database="postgres", user="postgres", password="postgres")

    def find_all_actions(self):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT * FROM "actions"'
            cur.execute(postgreSQL_select_Query)
            print("Selecting all rows from actions table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_setting = Action(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
                result_list.append(current_setting)

            return result_list

        finally:
            if cur:
                cur.close()

    def update_average_data(self, data, id):
        try:
            cur = self.session.cursor()
            query = 'UPDATE "actions" ' \
                    'SET "average_data" = \'' + str(data) + '\' ' \
                    + 'WHERE "basic_name" = \'' + id + '\''
            cur.execute(query)
            self.session.commit()

        finally:
             if cur:
                cur.close()

    def update_max_deviation(self, max, id):
        try:
            cur = self.session.cursor()
            query = 'UPDATE "actions" ' \
                    'SET "max_deviation" = \'' + str(max) +'\' ' \
                    + 'WHERE "basic_name" = \'' + id + '\''
            cur.execute(query)
            self.session.commit()

        finally:
            if cur:
                cur.close()

    def find_kids(self, id):
        try:
            cur = self.session.cursor()
            query = f'SELECT * FROM "actions" ' \
                    'WHERE "actions"."parent_action_id" = %s'
            cur.execute(query, (id,))
            kids = cur.fetchall()
            result_list = list()
            for row in kids:
                current_setting = Action(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
                result_list.append(current_setting)
            return result_list

        finally:
            if cur:
                cur.close()