import psycopg2
from injector import inject

from app.common.configuration import Configuration
from app.dal.entities.structure_types import StructureTypes


@inject
class StructureTypesRepository():
    #def __init__(self):
    def __init__(self, config: Configuration):
        self.session = psycopg2.connect(config.getConnectionString())
        #self.session = psycopg2.connect(database="test", user="postgres", password="12345")

    def find_all_structure_types(self):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT id, name FROM "structure_types"'
            cur.execute(postgreSQL_select_Query)
            print("Selecting all rows from structure_types table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_type = StructureTypes(row[0], row[1])
                result_list.append(current_type)
            return result_list
        finally:
            if (cur):
                cur.close()

    def find_structure_type_by_id(self, id):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT id, name FROM "structure_types" WHERE id = %s'
            cur.execute(postgreSQL_select_Query, (id,))
            print("Selecting all rows from structure_types table by id " + str(id) + " using cursor.fetchall")
            row = cur.fetchone()
            current_type = StructureTypes(row[0], row[1])
            return current_type
        finally:
            if (cur):
                cur.close()

    def find_structure_types_by_name(self, name):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT id, name FROM "structure_types" WHERE name = %s'
            cur.execute(postgreSQL_select_Query, (name,))
            print("Selecting all rows from structure_types by name " + str(name) + " table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_type = StructureTypes(row[0], row[1])
                result_list.append(current_type)
            return result_list
        finally:
            if (cur):
                cur.close()
