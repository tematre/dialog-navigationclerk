import psycopg2
from injector import inject

from app.common.configuration import Configuration
from app.dal.entities.setting import Setting


@inject
class SettingsRepository():
    def __init__(self):
        # def __init__(self, config: Configuration):
        # self.session = psycopg2.connect(config.getConnectionString())
        self.session = psycopg2.connect(database="test", user="postgres", password="12345")

    def find_all_settings(self):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT NAME, VALUE FROM "settings"'
            cur.execute(postgreSQL_select_Query)
            print("Selecting all rows from settings table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_setting = Setting(row[0], row[1])
                result_list.append(current_setting)

            return result_list

        finally:
            if (cur):
                cur.close()

    def find_settings_by_name(self, name):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT NAME, VALUE FROM "settings" WHERE name=?'
            cur.execute(postgreSQL_select_Query, (name,))
            print("Selecting rows by name from settings table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_setting = Setting(row[0], row[1])
                result_list.append(current_setting)

            return result_list

        finally:
            if (cur):
                cur.close()

    def find_settings_by_value(self, value):
        try:
            cur = self.session.cursor()
            postgreSQL_select_Query = 'SELECT NAME, VALUE FROM "settings" WHERE value=?'
            cur.execute(postgreSQL_select_Query, (value,))
            print("Selecting rows by value from settings table using cursor.fetchall")
            rows = cur.fetchall()
            result_list = list()
            for row in rows:
                current_setting = Setting(row[0], row[1])
                result_list.append(current_setting)

            return result_list

        finally:
            if (cur):
                cur.close()
