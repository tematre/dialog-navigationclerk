import uuid

from injector import inject

from app.dal.users_repository import UsersRepository


@inject
class UserService:
    def __init__(self, user_repository: UsersRepository):
        self.user_repository = user_repository

    def register_user(self, userDto):
        self.user_repository.create_user(userDto.userGuid, userDto.sex, userDto.age)
       

    def delete_user(self, guid):
        self.user_repository.delete_user_by_guid(guid)

    def find_user(self, guid):
        return self.user_repository.find_user_by_guid(guid)
