from injector import inject

from app.dal.structure_actions_repository import StructureActionsRepository
from app.dal.structure_items_repository import StructureItemsRepository


@inject
class StructureService(object):

    def __init__(self, strucActionsRep: StructureActionsRepository, strucItemsRep: StructureItemsRepository):
        self.strucActionsRep = strucActionsRep
        self.strucItemsRep = strucItemsRep
        self.structures = list()

    def findStructures(self, leaves):
        for leave in leaves:
            structure_actions_list = self.strucActionsRep.find_structure_actions_by_action_id(leave.basic_name)
            for structure_action in structure_actions_list:
                self.structures.append(self.strucItemsRep.find_structure_item_by_id(structure_action.structure_id))
        return self.structures