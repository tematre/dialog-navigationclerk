from injector import inject

from app.bll.bert_sentiment_model import BertSentimentModel
from app.bll.dialog_service import DialogService

@inject
class SentimentAnalysisService:
    @inject
    def __init__(self, dialog_service: DialogService, bert: BertSentimentModel):
        self.dialog_service = dialog_service
        self.bert = bert

    """
        This method recognizes sentiment level in given message string
        Parameters:
        message - object of class app.api.dto.MessageDto
    """

    def recognize_sentiment_value(self, message):
        # Need to check if the dialog is still active
        if self.dialog_service.is_dialog_expired(message.chatId):
            print('Dialog is expired! Reject to recognize action')
            return None

        predicted = self.bert.predict(message.message).numpy()
        print(predicted)
        return str(predicted[0][1]-predicted[0][2])
        #predicted = torch.argmax(predicted, dim=1).numpy()
