import torch
from injector import inject
import sys
import numpy
from app.common.configuration import Configuration
from app.dal.actions_repository import ActionsRepository
from transformers import AutoTokenizer, AutoModel

@inject
class SbertPreparationService(object):

    @inject
    def __init__(self, config: Configuration, repository: ActionsRepository):
        self.config = config
        self.actions_repository = repository
        self.model = AutoModel.from_pretrained("sberbank-ai/sbert_large_nlu_ru")
        self.tokenizer = AutoTokenizer.from_pretrained("sberbank-ai/sbert_large_nlu_ru")
        self.tolerance = 1.1

    def mean_pooling(self, model_output, attention_mask):
        token_embeddings = model_output[0]  # First element of model_output contains all token embeddings
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
        sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)
        return sum_embeddings / sum_mask

    def prepare_data(self):
        for action in self.actions_repository.find_all_actions():
            print(action.basic_name)
            samples = action.strategy_data.lower().split(';')
            num_of_samples = len(samples)
            average = torch.zeros(1, 1024)
            encoded_input = self.tokenizer(samples, padding=True, truncation=True, max_length=24, return_tensors='pt')
            with torch.no_grad():
                model_output = self.model(**encoded_input)
            sentence_embeddings = self.mean_pooling(model_output, encoded_input['attention_mask'])
            for s in sentence_embeddings:
                average = average.add(s.unsqueeze(0))
            average = average/num_of_samples

            max = 0
            for value in sentence_embeddings:
                distance = torch.cdist(value.unsqueeze(0), average).norm()
                if (distance > max):
                    max = distance
            print(average)
            numpy.set_printoptions(threshold=sys.maxsize)
            self.actions_repository.update_max_deviation(max.item() * self.tolerance, action.basic_name)
            self.actions_repository.update_average_data(str(average[0].detach().numpy())[1:-1], action.basic_name)