import torch
import numpy as np
from injector import inject
from transformers import BertModel, BertTokenizer


@inject
class BertStrategy:
    def __init__(self):
        self.model = BertModel.from_pretrained("bert-base-multilingual-uncased", return_dict=True)
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')

    def calculate_similarity(self, model_output_phrase, action):
        average = torch.from_numpy(np.expand_dims(np.fromstring(action.average_data, dtype=float, sep=' '), axis=0))
        max_deviation = action.max_deviation
        distance = torch.cdist(model_output_phrase.float(), average.float()).norm()
        if (distance <= max_deviation):
            return distance, True
        else:
            return distance, False

    def encode_input_phrase(self, input_phrase):
        input_phrase = input_phrase.lower()
        model = self.model
        tokenizer = self.tokenizer
        input_ids = tokenizer(input_phrase, return_tensors='pt')["input_ids"]
        model_output_phrase = model(input_ids)[0][:, 0, :]
        return model_output_phrase
