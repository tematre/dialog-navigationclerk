from injector import inject

from app.dal.actions_repository import ActionsRepository


@inject
class DecompositionService(object):

    def __init__(self, actionsRepository: ActionsRepository):
        self.tree = list()
        self.leaves = list()
        self.actionsRepository = actionsRepository

    def findLeaves(self, top_lvl_actions):
        result_list = list()

        for action in top_lvl_actions :
            kids = self.actionsRepository.find_kids(action.basic_name)
            if len(kids) > 0:
                for kid in kids:
                    for item in self.findLeaves(kid):
                        result_list.append(item)
            else:
                result_list.append(action)
        
        return result_list
