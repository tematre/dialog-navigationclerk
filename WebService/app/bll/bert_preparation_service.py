import torch
import numpy as np
from injector import inject

from app.common.configuration import Configuration
from app.dal.actions_repository import ActionsRepository
from transformers import BertModel, BertTokenizer

@inject
class BertPreparationService(object):
    # we'll use this to make max deviation bigger


    @inject
    def __init__(self, config: Configuration, repository: ActionsRepository):
        self.config = config
        self.actions_repository = repository
        self.model = BertModel.from_pretrained("bert-base-multilingual-uncased", return_dict=True)
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-uncased')
        self.tolerance = 1.1

    def prepare_data(self):
        """
            This method calculates average bert output CLS vector and max deviation
            for each element of "Actions" table, including non-bert strategy samples("bag", etc).
            Previous values will be overwritten.
        """
        for action in self.actions_repository.find_all_actions():
            print(action.basic_name)
            samples = action.strategy_data.lower().split(';')
            num_of_samples = len(samples)
            outputs = []
            average = torch.zeros(1, 768)
            for sample in samples:
                input_ids = self.tokenizer(sample, return_tensors='pt')["input_ids"]
                model_output = self.model(input_ids)[0][:, 0, :]
                average = average.add(model_output)
                outputs.append(model_output)
            # average value of model_output for current action
            average = average/num_of_samples

            # here we'll calculate maximum deviation from average vector
            max = 0
            for value in outputs:
                distance = torch.cdist(value, average).norm()
                if (distance > max):
                    max = distance

            # thus we have all data for future recognition, time to save it
            self.actions_repository.update_max_deviation(max.item() * self.tolerance, action.basic_name)
            self.actions_repository.update_average_data(str(average[0].detach().numpy())[1:-1], action.basic_name)
