from datetime import datetime

from injector import inject

from app.bll.decomposition_service import DecompositionService
from app.common.configuration import Configuration
from app.dal.dialogs_repository import DialogsRepository
from app.dal.entities.dialog_message_response_type import DialogMessageResponseType

@inject
class DialogService(object):
    def __init__(self, config: Configuration, dialogRepository: DialogsRepository, decompositionService: DecompositionService):
        self.config = config
        self.repository = dialogRepository
        self.decompositionService = decompositionService

    def is_dialog_expired(self, dialog_id):
        dialog = self.repository.find_dialog_by_id(dialog_id)
        if dialog.expire_date <= datetime.now():
            return True
        return False

    def get_current_open_dialog(self, user_id):
        dialog = self.repository.find_last_dialog_by_user_id(user_id)
        if dialog.expire_date <= datetime.utcnow():
            return None
        return dialog

    def start_new_dialog(self, user_id):
        self.repository.create_new_dialog(user_id, datetime.utcnow() + datetime.timedelta(minutes = 30))


    def close_dialog(self, dialog_id):
        self.repository.update_dialog_state(dialog_id, 2)

    def add_dialog_message(self, dialog_id, text, response_type, intent_id):
        self.repository.save_dialog_message(dialog_id, text, type)
        if response_type == DialogMessageResponseType.IntentRecognized:
            self.repository.save_dialog_message_action(dialog_id, intent_id)

    def get_dialog_intents(self, dialog_id):
        top_level_intents = self.repository.find_dialog_top_level_intents(dialog_id)

        decomposed = self.decompositionService.findLeaves(top_level_intents)
        return decomposed
