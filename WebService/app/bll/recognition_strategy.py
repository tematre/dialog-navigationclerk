"""
    Package with different recognition strategies.
    Although not yet required, but strongly recommended to keep
    method signatures similar amongst all of the strategies defined in this file.
"""

from enum import Enum



class Strategy(Enum):
    BAG_OF_WORDS = 'bag'
    BERT = 'bert'
    SBERT = 'sbert'
    RUBERT = 'rubert'


class BagOfWordsStrategy:
    @staticmethod
    def calculate_similarity(input_phrase, action):
        similarity = 0
        # Splitting input message into searchable parts
        tokens = input_phrase.lower().split()
        for phrase in action.strategy_data.lower().split(';'):
            for tok in tokens:
                if phrase.__contains__(tok):
                    similarity += 1
        return similarity, False
