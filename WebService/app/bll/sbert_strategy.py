import torch
import numpy as np
from injector import inject
from transformers import AutoModel, AutoTokenizer



@inject
class SbertStrategy(object):
    def __init__(self):
        self.model = AutoModel.from_pretrained("sberbank-ai/sbert_large_nlu_ru")
        self.tokenizer = AutoTokenizer.from_pretrained("sberbank-ai/sbert_large_nlu_ru")

    def calculate_similarity(self, model_output_phrase, action):
        average = torch.from_numpy(np.expand_dims(np.fromstring(action.average_data, dtype=float, sep=' '), axis=0))
        max_deviation = action.max_deviation
        distance = torch.cdist(model_output_phrase.float(), average.float()).norm()
        if (distance <= max_deviation):
            return distance, True
        else:
            return distance, False

    def encode_input_phrase(self, input_phrase):
        input_phrase = input_phrase.lower()
        encoded_input = self.tokenizer(input_phrase, padding=True, truncation=True, max_length=24, return_tensors='pt')
        with torch.no_grad():
            model_output = self.model(**encoded_input)
        model_output_phrase = self.mean_pooling(model_output, encoded_input['attention_mask'])
        return model_output_phrase


    def mean_pooling(self, model_output, attention_mask):
        token_embeddings = model_output[0]  # First element of model_output contains all token embeddings
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
        sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)
        return sum_embeddings / sum_mask