import math

import numpy as np
import six
import sys

from injector import inject

from app.dal.structure_items_repository import StructureItemsRepository

sys.modules['sklearn.externals.six'] = six
import mlrose


@inject
class DijkstraService:
    def __init__(self, structure_items_repository: StructureItemsRepository):
        self.structure_items_repository = structure_items_repository
        return

    def get_ordered_path(self, structures):
        distances = list()
        pairs = dict()
        index = 0
        structures_copy = structures.copy()
        for structure_1 in structures_copy:
            for structure_2 in structures_copy:
                if structure_1 != structure_2:
                    index = self.connect_two_structures(structure_1, structure_2, distances, pairs, index)
            structures_copy.remove(structure_1)
        print(distances)
        fitness_coords = mlrose.TravellingSales(distances=distances)
        problem_fit = mlrose.TSPOpt(length=len(structures), fitness_fn=fitness_coords, maximize=False)
        best_state, best_fitness = mlrose.genetic_alg(problem_fit, random_state=2)
        result = list()
        key_list = list(pairs.keys())
        value_list = list(pairs.values())
        for i in best_state:
            position = value_list.index(i)
            result.append(key_list[position])
        print(result)
        return result

    def connect_two_structures(self, structure_1, structure_2, distances, pairs, index):
        if not pairs.__contains__(structure_1.id):
            pairs[structure_1.id] = index
            index = index + 1

        if not pairs.__contains__(structure_2.id):
            pairs[structure_2.id] = index
            index = index + 1

        cur_distance = self.distance_between_two_structures(structure_1, structure_2)
        distances.append((pairs[structure_1.id], pairs[structure_2.id], cur_distance))
        return index

    def distance_between_two_structures(self, structure_1, structure_2):
        cur_distance = 0
        if structure_1.parent_id != structure_2.parent_id:
            parents_1 = self.get_all_parents(structure_1)
            parents_2 = self.get_all_parents(structure_2)
            for parent in parents_1:
                if parents_2.__contains__(parent):
                    index_1 = parents_1.index(parent)
                    index_2 = parents_2.index(parent)
                    max_index = max(index_1, index_2)
                    cur_distance = 100 * (10 ** max_index)
                    if structure_1.order_num is None or structure_2.order_num is None:
                        return cur_distance
                    order_distance = math.fabs(structure_1.order_num - structure_2.order_num)
                    cur_distance = cur_distance + order_distance
                    return cur_distance
            cur_distance = 100 * (10 ** 5)  # TODO добавить максимальную дистанцию
            return cur_distance

        # TODO если у них нет order_num
        order_distance = math.fabs(structure_1.order_num - structure_2.order_num)
        cur_distance = cur_distance + order_distance
        return cur_distance

    def get_all_parents(self, structure):
        parents = list()
        while structure.parent_id is not None:
            parents.append(structure.parent_id)
            structure = self.structure_items_repository.find_structure_item_by_id(structure.parent_id)
        return parents
