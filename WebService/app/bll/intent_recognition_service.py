from injector import inject

from app.bll.bert_strategy import BertStrategy
from app.bll.dialog_service import DialogService
from app.bll.recognition_strategy import *
from app.bll.rubert_strategy import RubertStrategy
from app.bll.sbert_strategy import SbertStrategy
from app.common.configuration import Configuration
from app.dal.actions_repository import ActionsRepository


@inject
class IntentRecognitionService(object):
    """
    docstring
    """

    @inject
    def __init__(self, config: Configuration, dialog_service: DialogService, repository: ActionsRepository,
                 bert: BertStrategy, sbert: SbertStrategy, rubert: RubertStrategy):
        self.config = config
        self.repository = repository
        self.dialog_service = dialog_service
        self.isBertable = False
        self.input_phrase_encoded = 0
        self.bertStrategy = bert
        self.sbertStrategy = sbert
        self.rubertStrategy = rubert

    """
        This method recognises action in given message string
        Parameters:
            message - object of class app.api.dto.MessageDto
    """

    def recognize_action(self, message):
        way = "bert"
        # Need to check if the dialog is still active
        if self.dialog_service.is_dialog_expired(message.chatId):
            print('Dialog is expired! Reject to recognize action')
            return None

        input_phrase = message.message
        if(way == "bert"):
            self.input_phrase_encoded = self.bertStrategy.encode_input_phrase(input_phrase)
        if(way == "sbert"):
            self.input_phrase_encoded = self.sbertStrategy.encode_input_phrase(input_phrase)
        if(way == "rubert"):
            self.input_phrase_encoded = self.rubertStrategy.encode_input_phrase(input_phrase)
        max_bag_similarity = 0  # Number to choose action. Greater the number is - more likely action will be chosen
        max_similarity = 99999
        chosen_action = ''
        # Searching the most relevant action
        for action in self.repository.find_all_actions():
            similarity, isBertable = self.calculate_similarity(input_phrase, action)
            if(isBertable):
                self.isBertable = isBertable

            if(self.isBertable):
                if (max_similarity > similarity):
                    max_similarity = similarity
                    chosen_action = action
            else:
                similarity, flag = BagOfWordsStrategy.calculate_similarity(input_phrase, action)
                if similarity > max_bag_similarity:
                    max_bag_similarity = similarity
                    chosen_action = action
        return chosen_action

    def calculate_similarity(self, input_phrase, action):
        if action.strategy.__eq__(Strategy.BAG_OF_WORDS.value):
            return BagOfWordsStrategy.calculate_similarity(input_phrase, action)
        elif action.strategy.__eq__(Strategy.BERT.value):
            return self.bertStrategy.calculate_similarity(self.input_phrase_encoded, action)
        elif action.strategy.__eq__(Strategy.SBERT.value):
            return self.sbertStrategy.calculate_similarity(self.input_phrase_encoded, action)
        elif action.strategy.__eq__(Strategy.RUBERT.value):
            return self.rubertStrategy.calculate_similarity(self.input_phrase_encoded, action)
        else:
            print(f'Unknown strategy {action.strategy}!')
            return -1
