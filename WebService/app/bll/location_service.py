from injector import inject
from random import randint


@inject
class LocationService(object):

    def __init__(self):
        self.dict_locations = dict()

    def set_location(self, id, room):
        self.dict_locations.update(id, room)

    def get_location(self, id):
        if id in self.dict_locations:
            return self.dict_locations.get(id)
        else:
            room = randint(1, 100)
            self.set_location(id, room)
            self.get_location(id)


