import torch
from transformers import AutoModelForSequenceClassification
from transformers import BertTokenizerFast
from injector import inject

@inject
class BertSentimentModel:
    def __init__(self):
        self.model = AutoModelForSequenceClassification.from_pretrained('blanchefort/rubert-base-cased-sentiment', return_dict=True)
        self.tokenizer = BertTokenizerFast.from_pretrained('blanchefort/rubert-base-cased-sentiment')

    @torch.no_grad()
    def predict(self, text):
        inputs = self.tokenizer(text, max_length=512, padding=True, truncation=True, return_tensors='pt')
        outputs = self.model(**inputs)
        predicted = torch.nn.functional.softmax(outputs.logits, dim=1)
        return predicted
