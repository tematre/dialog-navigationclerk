from flask import Blueprint, request
from injector import inject

from app.api.dto.message import MessageDto
from app.bll.intent_recognition_service import IntentRecognitionService
from app.bll.sentiment_analysis_service import SentimentAnalysisService
from app.bll.dialog_service import DialogService

from app.bll.user_service import UserService
from app.api.dto.message_response import MessageResponse
from app.api.dto.message_response_type import MessageResponseType
from app.bll.structure_service import StructureService
from app.bll.dijkstra_service import DijkstraService

from app.dal.entities.dialog_message_response_type import DialogMessageResponseType

from app.api.dto.user import UserDto

dialog_api = Blueprint('dialog_api', __name__, url_prefix='/dialog_api')


@inject
@dialog_api.route("/dialog", methods=['POST'])
def post_message_handle(intent_service: IntentRecognitionService, sentiment_service: SentimentAnalysisService, user_service: UserService, dialog_servie: DialogService, structures_service: StructureService, dijkstra_service: DijkstraService):
    content = request.get_json()
    message_dto = MessageDto(**content)
    # print(message_dto.message + message_dto.chatId + message_dto.userId)

    user = user_service.find_user(message_dto.userId);
    if user is None:
        user_service.register_user(UserDto(message_dto.userId, 'male', 18))
        user = user_service.find_user(message_dto.userId);

    dialog = dialog_servie.get_current_open_dialog();
    if dialog is None:
        dialog = dialog_servie.start_new_dialog(user.id)

    responseType = DialogMessageResponseType.ClarificationRequired

    if message_dto.message == 'закончить диалог':
        responseType =  DialogMessageResponseType.DialogEnded
        intents = dialog_servie.get_dialog_intents(dialog.id)
        structure_items = structures_service.findStructures(intents) 
        ordered_path = dijkstra_service.getget_ordered_path(structure_items)
    else:
        action = intent_service.recognize_action(message_dto)

    text = ''
    
    if action:
        text = 'Распознано действие: ' + action.basic_name + '. Что-нибудь еще?'
        responseType = DialogMessageResponseType.IntentRecognized
    else:
        text = "Пожалуйста, уточните запрос?"
        responseType = DialogMessageResponseType.ClarificationRequired

    dialog_servie.add_dialog_message(dialog.id, message_dto.message,responseType, action)

    



@inject
@dialog_api.route("/sentiment", methods=['POST'])
def post_message_sentiment(intent_service: IntentRecognitionService, sentiment_service: SentimentAnalysisService):
    content = request.get_json()
    message_dto = MessageDto(**content)
    # print(message_dto.message + message_dto.chatId + message_dto.userId)

    #action = intent_service.recognize_action(message_dto)
    sentiment_level = sentiment_service.recognize_sentiment_value(message_dto)
    action = "Unknown"
    if action:
        if sentiment_level:
            return "Recognized action: "+action+"\n"+"Sentiment: "+sentiment_level, 200
        #return action.basic_name, 200
    else:
        return 'Действие не распознано!', 200
