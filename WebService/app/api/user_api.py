from flask import Blueprint, request, json, jsonify
from injector import inject

from app.api.dto.user import UserDto
from app.bll.user_service import UserService

user_api = Blueprint('user_api', __name__, url_prefix='/user_api')


@inject
@user_api.route("", methods=['POST'])
def register_user(user_service: UserService):
    content = request.get_json()
    user_dto = UserDto(**content)
    return user_service.register_user(user_dto)


@inject
@user_api.route("/<user_id>", methods=['DELETE'])
def delete_user(user_service: UserService, user_id):
    user_service.delete_user(user_id)
    return "successful", 200


@inject
@user_api.route("/<user_id>", methods=['GET'])
def find_user(user_service: UserService, user_id):
    user = user_service.find_user(user_id)
    return jsonify(
        user_guid=user.guid,
        sex=user.sex,
        age=user.age
    )
