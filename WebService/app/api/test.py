from flask import Blueprint, request
from injector import inject

from app.api.dto.message import MessageDto
from app.bll.intent_recognition_service import IntentRecognitionService
from app.bll.sentiment_analysis_service import SentimentAnalysisService
from app.bll.dialog_service import DialogService

from app.bll.user_service import UserService
from app.api.dto.message_response import MessageResponse
from app.api.dto.message_response_type import MessageResponseType
from app.bll.structure_service import StructureService
from app.bll.dijkstra_service import DijkstraService

from app.dal.entities.dialog_message_response_type import DialogMessageResponseType

from app.api.dto.user import UserDto

test_api = Blueprint('test', __name__, url_prefix='/test')


@test_api.route("/",methods=['GET'])
def check():
    return "All works OK!"

