import enum
class MessageResponseType(enum.Enum):
    ClarificationRequired = 1
    IntentRecognized = 2