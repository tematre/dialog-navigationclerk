import json


class MessageResponse(object):
    def __init__(self):
        self.responseType = ''
        self.data = None

    def __init__(self, responseType, data):
        self.responseType = responseType
        self.data = data

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=False, indent=4)
