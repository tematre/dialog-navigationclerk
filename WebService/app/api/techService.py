from flask import Blueprint, request
from injector import inject

#api to run some commands to process database data and etc
from app.bll.bert_preparation_service import BertPreparationService
from app.bll.decomposition_service import DecompositionService
from app.bll.rubert_preparation_service import RubertPreparationService
from app.bll.dijkstra_service import DijkstraService
from app.bll.sbert_preparation_service import SbertPreparationService
from app.bll.structure_service import StructureService
from app.dal.entities.actions import Action
from app.dal.entities.structure_items import StructureItems

tech_api = Blueprint('tech_api', __name__, url_prefix='/tech_api')

"""
This controller is used to run "technical" services like bert_preparation_service and ect
"""
@inject
@tech_api.route("/bert_data_preprocess/<message>")
def bert_data_preprocess(message, bertservice: BertPreparationService, sbertservice: SbertPreparationService,
                         rubertservice: RubertPreparationService):
    print(message)
    if(message == "bert"):
        bertservice.prepare_data()
    if(message == "sbert"):
        sbertservice.prepare_data()
    if(message == "rubert"):
        rubertservice.prepare_data()
    return "Ok", 200
"""
just2test decomposition_service
"""
@inject
@tech_api.route("/decomposition_service/<message>")
def knockToFindTree(message, actionsService: DecompositionService, structuresService: StructureService, dijkstraService: DijkstraService):
    print(message)
    testAction = Action(basic_name="Оплатить обучение", parent_action_id=None, priority=None, strategy=None, strategy_data=None,
          average_data=None, max_deviation=None)
    tree, leaves = actionsService.findTreeAndLeaves(testAction)
    # for action in tree:
    #     print(action.basic_name)
    # print("that's all for now")
    for leave in leaves:
        print(leave.basic_name)

    structures = structuresService.findStructures(leaves)
    for structure in structures:
        print(structure.id)
    structures.append(StructureItems(3, 2, 2, 1))
    print(dijkstraService.get_distance(structures))
    return 'coolest service ever', 200