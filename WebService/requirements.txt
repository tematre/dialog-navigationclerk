certifi==2020.11.8
chardet==3.0.4
click==7.1.2
docker==4.3.1
Flask==1.1.2
Flask-Injector==0.12.3
Flask-Script==2.0.6
Flask-Scrypt==0.1.3.6
idna==2.10
injector==0.18.4
itsdangerous==1.1.0
Jinja2==2.11.2
MarkupSafe==1.1.1
psycopg2-binary==2.8.6
python-configuration==0.8.1
requests==2.25.0
scrypt==0.8.17
six==1.15.0
sqlparse==0.4.1
tabulate==0.8.7
typing-extensions==3.7.4.3
urllib3==1.26.2
websocket-client==0.57.0
Werkzeug==1.0.1
yoyo-migrations==7.2.1
mlrose
numpy
transformers
torch