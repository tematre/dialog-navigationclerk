import os
import setuptools

long_description = ''
install_requires = [] 

with open("README.md", "r") as fh:
    long_description = fh.read()
    
with open("requirements.txt", "r") as requirementsFile:
        install_requires = requirementsFile.read().splitlines()

setuptools.setup(
    name="dialog-webservice", 
    version="0.0.1",
    author="NSU Strudents",
    author_email="author@example.com",
    description="NavigationClerk web service used by Dialog system",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    nstall_requires=install_requires
)