echo 'Remove VENV'
rd /s /q .\venv

echo 'Create VENV'
python -m venv venv
set FLASK_APP=%VIRTUAL_ENV%/../app/run.py
set DEBUG='True'
set LIB=C:\Program Files (x86)\OpenSSL-Win32\lib;C:\Program Files\OpenSSL-Win32\lib;C:\Program Files (x86)\OpenSSL-Win64\lib;C:\Program Files\OpenSSL-Win64\lib;%LIB%
set INCLUDE=C:\Program Files (x86)\OpenSSL-Win32\include;C:\Program Files\OpenSSL-Win32\include;C:\Program Files (x86)\OpenSSL-Win64\include;C:\Program Files\OpenSSL-Win64\include;%INCLUDE%

cmd /k ".\venv\Scripts\activate.bat & python -m pip install --upgrade pip & pip install -r requirements.txt"
