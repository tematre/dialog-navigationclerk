echo 'Remove VENV'
rd /s /q .\venv

echo 'Create VENV'
python -m venv venv
set DEBUG='True'
set LIB=C:\Program Files (x86)\OpenSSL-Win32\lib;C:\Program Files\OpenSSL-Win32\lib;%LIB%
set INCLUDE=C:\Program Files (x86)\OpenSSL-Win32\include;C:\Program Files\OpenSSL-Win32\include;%INCLUDE%

cmd /k ".\venv\Scripts\activate.bat & python -m pip install --upgrade pip & pip install -r requirements.txt"
