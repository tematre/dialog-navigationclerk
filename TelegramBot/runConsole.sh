#!/usr/bin/env bash
echo 'Remove VENV'
rm -rf ./venv

echo 'Create VENV'
python3 -m venv venv
export DEBUG='True'

echo 'Activating VENV'
$SHELL -c    "source ./venv/bin/activate; 
                pip install --upgrade pip; 
                pip install -r requirements.txt -v; 
                exec $SHELL -i"