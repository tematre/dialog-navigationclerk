import logging
import requests
import json
from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '1435900925:AAGVHSqf1VX1OdrjzaGQv9hDUfT3dDmmWJU'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

@dp.message_handler(commands='start')
async def start_cmd_handler(message: types.Message):
    """
    This handler will be called when user sends `/start` command
    """

    await message.answer("Здравствуйте, я ваш навигационный ассистент и постараюсь вам помочь")

@dp.message_handler(commands=['help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/help` command
    """
    await message.reply("Здравствуйте, я ваш навигационный ассистент и постараюсь вам помочь.\nПросто скажите, что вам нужно сделать")


@dp.message_handler()
async def echo(message: types.Message):

    #body = ("""{"userId":\""""+str(message.from_user.id)+"""\","chatId":\""""+str(message.chat.id)+"""\","message":\""""+
    #        str(message.text)+"""\"}""")
    body = ("""{"userId":\"""" + str(message.from_user.id) + """\","message":\"""" + str(message.text) + """\"}""")
    print(body)
    body_json = json.loads(body)

    json_headers = {'Content-type': 'application/json'}

    r = requests.post('http://localhost:5000/dialog_api/dialog', headers=json_headers, json=body_json, verify=False)

    print(r.status_code)
    print(r.content)
    print(f"url: {r.url} \n\ntext: \n {r.text}")

    #if r.status_code == 200:
        #json_answer = json.loads(r.content)
        #print(json_answer)
        #response = MessageResponse(**json_answer)
        #answer_text = response.data
    #else:
        #answer_text = "Sorry, server error.\nPlease, try again"

    await message.answer(r.text)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)